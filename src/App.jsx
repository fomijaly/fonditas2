import "./App.css";

import Header from "./components/Header.jsx";

import { favoritasList, fondasList, menus } from "./data.js";

import TitlesCategories from "./components/TitlesCategories.jsx";

import CategoryFavoritas from "./components/CategoryFavoritas.jsx";
import CategoryFondas from "./components/CategoryFondas";
import CategoryMenus from "./components/CategoryMenus";

function App() {
  return (
    <>
      <Header />
      <main className="main__categories__container">
        <section>
          <TitlesCategories tag="CATEGORÍAS" titleName="Las favoritas" />
          <div className="allFavoritas__container">
            {favoritasList.map((currentElement, index) => (
              <CategoryFavoritas key={index} {...currentElement} />
            ))}
          </div>
        </section>
        <section>
          <TitlesCategories
            tag="FONDITAS"
            titleName="Fondas cercanas"
            subText="Estás son las fondas que se encuentran cerca!"
          />
          <div className="CategoryFondas">
            {fondasList.map((currentElement, index) => (
              <CategoryFondas key={index} {...currentElement} />
            ))}
          </div>
        </section>
        <section>
          <TitlesCategories
            tag="MENÚS"
            titleName="Los mejores menús"
            subText="Aquí están los mejores menús de la semana, y decide que vas a pedir"
          />
          <div className="menus__box">
            {menus.map((currentElement, index) => (
              <CategoryMenus key={index} {...currentElement} />
            ))}
          </div>
        </section>
      </main>
    </>
  );
}

export default App;
