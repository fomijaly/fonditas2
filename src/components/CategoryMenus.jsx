import './CategoryMenus.css'
import star from '../assets/star.svg'

function CategoryMenus(props){
    const {name, image, category, include, time, price, review} = props;
    return(
            <section className='card__menus'>
                <img src={image} alt={name} />
                <div className='card__menus_details'>
                    <span>{category}</span>
                    <h4>{name}</h4>
                    <p className='card__menus_include'>{`Incluye ${include}`}</p>
                    <div className='card__menus_details-row'>
                        <p>{`${time} min`}</p>
                        <p>{`Desde $${price}`}</p>
                    </div>
                </div>
                <span className='card__menus_review'>
                    <img src={star} alt="Avis" />
                    {review}
                </span>
            </section>
    )
}

export default CategoryMenus