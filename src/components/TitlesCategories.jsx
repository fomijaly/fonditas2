import "./TitlesCategories.css";

export default function TitlesCategories(props) {
    const {tag, titleName, subText} = props;
  return (
    <div className="titles__container">
        <p className="titles__greenTag">{tag}</p>
        <h3 className="titles__title">{titleName}</h3>
        <p className="titles__subtext">{subText}</p>
    </div>
  )
}
