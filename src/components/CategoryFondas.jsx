import "./CategoryFondas.css";

export default function CategoryFondas(props) {
  const { star, rating, img, title, duration, type } = props;
  return (
    <section className="Fondas__container">
      <div className="Fondas__ratingContainer">
        <img className="rating-star" src={star} alt={rating} />
        <p className="Fondas__rating">{rating}</p>
      </div>
      <img className="Fondas__img" src={img} alt={title}></img>
      <div className="Fondas__infosContainer">
        <h2 className="Fondas__title">{title}</h2>
        <div className="Fondas__infosContainer__duration-type">
          <h4 className="Fondas__duration">{duration} min</h4>
          <p className="Fondas__type">{type}</p>
        </div>
      </div>
    </section>
  );
}
